function handle_api_errors(response) {

    // if jwt expired / invalid or does not exist
    if (response.status == 403 || response.status == 422) {
        window.location.href = "/login.html";
    } else {
        alert("ERROR:\n" + response.json());
    }

}

async function fetchtodos_get() {

    let response = await fetch("/api/todo")
    let parsedJson = await response.json();

    if (response.status == 200) {
        return parsedJson.response
    } else {
        handle_api_errors(response);
    }

}

async function fetchtodos_post(text_lst) {

    let fetchdata = {
        method: 'POST',
        body: JSON.stringify({data: text_lst}),
        headers: {
            'Content-Type': 'application/json'
          }
    }

    let response = await fetch("/api/todo", fetchdata);
    let parsedJson = await response.json();

    if (response.status == 201) {
        return parsedJson.response
    } else {
        handle_api_errors(response);
    }

}

async function fetchtodos_delete(dellist) {

    let fetchdata = {
        method: 'DELETE',
        body: JSON.stringify({data: dellist}),
        headers: {
            'Content-Type': 'application/json'
          }
    }

    let response = await fetch("/api/todo", fetchdata);

    if (response.status != 200) {
        handle_api_errors(response);
    }

}

async function deletetodos() {

    let tablebody = document.getElementById("main-table").getElementsByTagName("tbody")[0];
    
    todo_todelete = [];
    for (let i = 0; i < tablebody.children.length; i++) {
        let row = tablebody.children[i];
        let checkbox = row.firstElementChild.firstElementChild;
        if (checkbox.checked) {
            todo_todelete.push(row.id);
        }
    }

    await fetchtodos_delete(todo_todelete);

    for (let i = 0; i < todo_todelete.length; i++) {
        document.getElementById(todo_todelete[i]).remove();

    }
    table_uncheck();

}

function todo_totable(todo_item) {

    let tablebody = document.getElementById("main-table").getElementsByTagName("tbody")[0];
 
    let row = tablebody.insertRow();

    // create table row checkbox
    let checkbox_cell = row.insertCell();
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.onclick = table_check;
    checkbox_cell.appendChild(checkbox);
    
    // create table row todo text
    let todo_cell = row.insertCell();
    let todo_text = document.createTextNode(todo_item.text);
    todo_cell.appendChild(todo_text);

    row.id = todo_item.id;

}

async function newtodo() {
    let todo_input = document.getElementById("todo-input");

    let todos = await fetchtodos_post([todo_input.value,]);

    for (let i = 0; i < todos.length; i++) {
        todo_totable(todos[i]);
    }

    todo_input.value = "";

}

async function populatetable() {
    todos = await fetchtodos_get();

    let tablebody = document.getElementById("main-table").getElementsByTagName("tbody")[0];

    for (let i = 0; i < todos.length; i++) {
        todo_totable(todos[i])
    }

}

function table_toggleall() {
    let headcheck = document.getElementById("headcheck");
    let tablebody = document.getElementById("main-table").getElementsByTagName("tbody")[0];

    // iterate over all body rows and toggle checkmark
    totalchecked = 0;
    for (let i = 0; i < tablebody.children.length; i++) {
        let checkbox = tablebody.children[i].firstElementChild.firstElementChild;
        checkbox.checked = headcheck.checked;
        if (headcheck.checked) {
            totalchecked++;
        }
    }

    document.getElementById("del-btn").disabled = !headcheck.checked;
}

let totalchecked = 0;
function table_check() {
    
    let headcheck = document.getElementById("headcheck");
    headcheck.checked = false;

    if (this.checked == true) {
        totalchecked++;
    } else if (this.checked == false) {
        totalchecked--;
    }

    if (totalchecked == 0) {
        document.getElementById("del-btn").disabled = true;
    } else {
        document.getElementById("del-btn").disabled = false;
    }

}

function table_uncheck() {
    let tablebody = document.getElementById("main-table").getElementsByTagName("tbody")[0];
    
    let headcheck = document.getElementById("headcheck");
    headcheck.checked = false;

    totalchecked = 0;
    for (let i = 0; i < tablebody.children.length; i++) {
        let checkbox = tablebody.children[i].firstElementChild.firstElementChild;
        checkbox.checked = false;
    }

    document.getElementById("del-btn").disabled = true;

}

populatetable();

// enter key submits new todo item
let todo_input = document.getElementById("todo-input");
todo_input.addEventListener("keyup", function(event) {
    if (event.code == "Enter") {
        event.preventDefault();
        document.getElementById("add-btn").click();
    }
});

document.getElementById("headcheck").onclick = table_toggleall;

document.getElementById("del-btn").onclick = deletetodos;
document.getElementById("add-btn").onclick = newtodo;

// fix checkbox still being checked on page reload
table_check();
document.getElementById("del-btn").disabled = true;