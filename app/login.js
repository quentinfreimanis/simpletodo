async function loginuser() {

    const uname = document.getElementById("username-input").value;
    const passwd = document.getElementById("password-input").value;

    const data = {
        method: 'POST',
        body: JSON.stringify({username: uname, password: passwd}),
        headers: {
            'Content-Type': 'application/json'
          }
    };

    const response = await fetch("api/login", data);
    const parsedJson = await response.json();

    if (response.status == 200) {
        window.location.href = "/";
    } else {
        console.log(parsedJson);
        alert(parsedJson.description);
    }

}


window.onload = function() {

    const loginform = document.getElementById("login-form");
    loginform.onsubmit = loginuser;

}
